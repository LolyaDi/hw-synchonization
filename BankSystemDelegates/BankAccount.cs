﻿using System;
using System.Runtime.Remoting.Contexts;

namespace BankSystemDelegates
{
    public delegate void ReporterDelegate(string text);

    [Synchronization]
    public class BankAccount : ContextBoundObject
    {
        public event ReporterDelegate ReportEvent;

        public string FullName { get; set; }
        public int Sum { get; private set; } = 0;

        public void Add(object sum)
        {
            Sum += (int)sum;
            ReportEvent.Invoke($"Вам на счет поступила сумма: {sum} тенге. Теперь у Вас на счете: {Sum} тенге.");
        }

        public void Withdraw(object sum)
        {
            if ((int)sum <= Sum)
            {
                Sum -= (int)sum;
                ReportEvent.Invoke($"С вашего счета снята сумма: {sum} тенге. Теперь у Вас на счете: {Sum} тенге.");
                return;
            }

            ReportEvent.Invoke("У вас недостаточно средств");
        }
    }
}
