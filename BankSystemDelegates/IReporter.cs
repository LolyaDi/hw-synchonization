﻿namespace BankSystemDelegates
{
    public interface IReporter
    {
        void SendReport(string message);
    }
}
